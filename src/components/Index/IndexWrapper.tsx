import styled from "styled-components";
import Progress from "./Progress";
import TodoListContainer from "./TodoListContainer";

const StyledIndexWrapper = styled.div`
  background-color: #f5f5f5;
  max-width: 720px;
  margin: 0 auto;
  padding: calc(var(--global-space)*3) calc(var(--global-space)*5);
  border-radius: 20px;

  @media screen
  and (max-width: 580px) {
    padding: calc(var(--global-space)*1);
  }
`;

const IndexWrapper = () => {

  return (<StyledIndexWrapper>
    <Progress />
    <TodoListContainer />
  </StyledIndexWrapper>)
}

export default IndexWrapper