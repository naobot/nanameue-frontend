import styled from "styled-components";
import { ChangeEvent, Dispatch, MouseEventHandler, SetStateAction, useContext, useEffect, useState } from "react";
import { TodosContext } from "../Index";
import { ErrorResponse, Todo } from "../../shared/interfaces";
import { useApiCallback } from "../../shared/hooks";
import NotificationWindow from "../NotificationWindow";

interface TodoListProps {
  todosData: Todo[];
  // todosRefresh: (() => Promise<void>) | undefined;
}
interface TodoItemProps {
  todo: Todo;
  // refresh: (() => Promise<void>) | undefined;
}
interface DropdownFilterProps {
  currentFilter: 'All' | 'Done' | 'Undone';
  setCurrentFilter: Dispatch<SetStateAction<'All' | 'Done' | 'Undone'>>
}

const StyledTodoListContainer = styled.div`
  .todoList__header {
    display: grid;
    grid-template-columns: 1fr max-content;
    align-items: center;
    margin-top: calc(var(--global-space)*1);
    grid-gap: calc(var(--global-space)*1);
    gap: calc(var(--global-space)*1);
  }

  .todoList__info {
    text-align: center;
    margin-top: calc(var(--global-space)*1);
  }

  .todoList__dropdownFilter {
    position: relative;
    width: 110px;
    font-family: Roboto;
    font-style: normal;
    font-weight: 400;
    color: #000;
    a.filterDefault {
      display: block;
      position: relative;
      background: #fff;
      height: 29px;
      align-content: center;
      display: grid;
      border-radius: 10px;
      padding: calc(var(--global-space) * .5);
      border-radius: 10px;
      background-image: url(/images/down_arrow.svg);
      background-repeat: no-repeat;
      background-position: calc(100% - 10px);
      background-size: 14px;
      font-size: 13px;
    }
    .todoList__dropdownFilterList {
      display: grid;
      position: absolute;
      z-index: 2;
      top: calc(100% + var(--global-space)*.25);
      right: 0;
      width: 100%;
      padding: calc(var(--global-space)*.25);
      transform: translateY(-4px);
      transition: opacity .2s,transform .2s;
        transition-timing-function: ease, ease;
      transition-timing-function: ease-in-out;
      border-radius: 10px;
      opacity: 0;
      background-color: #fff;
      box-shadow: 0 0 8px rgba(0,0,0,.1);
      pointer-events: none;
      will-change: opacity,transform;
      &.active {
        transform: translateY(0);
        opacity: 1;
        a {
          pointer-events: auto;
        }
      }
      a {
        padding: calc(var(--global-space)*.25) calc(var(--global-space)*.5);
        border-radius: 8px;
        font-size: 14px;
        &.active {
          background-color: #526f92;
          color: #fff;
        }
      }
    }
  }
  .todoList__dropdownFilterOverlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transition: opacity .4s, background-color .4s;
    z-index: 1;
    opacity: 0;
    background-color: transparent;
    pointer-events: none;
    &.active {
      opacity: 1;
      pointer-events: auto;
      cursor: pointer;
      background-color: rgba(0,0,0,0);
    }
  }

  h1 {
    color: #000;
    font-family: Roboto;
    font-size: 24px;
    font-style: normal;
    font-weight: 500;
  }

  .todoItem__form_addTodo {
    display: grid;
    grid-template-columns: 1fr max-content;
    margin-top: calc(var(--global-space)*1);
    padding: calc(var(--global-space)*.25);
    padding-left: calc(var(--global-space)*1);
    border-radius: 9999px;
    background-color: #fff;
    grid-gap: calc(var(--global-space)*.5);
    gap: calc(var(--global-space)*.5);
    transition: opacity .4s;
    opacity: 1;
    &.disabled {
      opacity: 0.6;
      pointer-events: none;
    }
    input {
      color: #525252;
      display: block;
      width: 100%;
      padding: 0;
      border: none;
      border-radius: 0;
      outline: none;
      background-color: transparent;
      box-shadow: none;
      font-family: Roboto;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
    }
    button {
      display: block;
      width: 100%;
      padding: calc(var(--global-space)*.4) var(--global-space);
      border: none;
      border-radius: 999px;
      background-color: #526f92;
      box-shadow: none;
      color: #fff;
      font-family: Roboto;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
    }
  }

  .todoItem__dl_item {
    display: grid;
    grid-template-columns: max-content 1fr max-content;
    align-items: center;
    padding: calc(var(--global-space)*.5) calc(var(--global-space)*1);
    transition: opacity .4s;
      transition-timing-function: ease;
    transition-timing-function: ease-in-out;
    border-radius: 9999px;
    background-color: #fff;
    gap: calc(var(--global-space)*1);
    font-family: Roboto;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    opacity: 0;
    &.active {
      opacity: 1;
    }
    &.disabled {
      pointer-events: none;
      opacity: 0.6;
    }
    input {
      display: block;
      width: 22px;
      height: 22px;
      padding: 0;
      border: 2px solid #526f92;
      border-radius: 6px;
      background-color: transparent;
      box-shadow: none;
      cursor: pointer;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      &:disabled {
        opacity: 0.6;
        pointer-events: none;
      }
      &:checked {
        background-color: #526f92;
        background-image: url(/images/checkbox.svg);
        background-repeat: no-repeat;
        background-position: 50%;
        background-size: 11px 9px;
      }
    }

    .todoName {
      color: #2e2e2e;
    }
    .todoName__done {
      color: #a9a9a9;
      text-decoration: line-through;
    }
  }
  dl {
    display: grid;
    margin-top: calc(var(--global-space)*.5);
    grid-gap: calc(var(--global-space)*.5);
    gap: calc(var(--global-space)*.5);
  }
  .todoItem__loading {
    margin-left: 1rem;
  }
  .todoItem__dl_item__options {
    position: relative;
    a {
      img {
        display: block;
        max-width: 100%;
        border: none;
      }
    }
  }
  .todoItem__deleteMenu {
    position: absolute;
    z-index: 2;
    right: 0;
    padding: calc(var(--global-space)*.5) calc(var(--global-space)*1);
    transform: translateY(-4px);
    transition: opacity .2s,transform .2s;
      transition-timing-function: ease, ease;
    transition-timing-function: ease-in-out;
    border-radius: 10px;
    opacity: 0;
    background-color: #fff;
    box-shadow: 0 0 8px rgba(0,0,0,.1);
    line-height: 1;
    pointer-events: none;
    will-change: opacity,transform;
    &.active {
      opacity: 1;
      transform: translateY(0);
      pointer-events: auto;
    }
    a {
      color: #e07c7c;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      white-space: nowrap;
    }
  }
  .errorMessage {
    background: #fff;
    padding: calc(var(--global-space)*.5) calc(var(--global-space)*1);
    border: 1px solid #e07c7c;
    border-radius: 10px;
    color: #e07c7c;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    white-space: nowrap;
  }
`;

const AddTodoForm = () => {
  const [todoText, setTodoText] = useState<string>('');
  const [addInProgress, setAddInProgress] = useState(false);
  const [addTodoError, setAddTodoError] = useState<string|null>(null);

  const { getUpdatedTodos } = useContext(TodosContext) || { getUpdatedTodos: null };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTodoText(e.target.value);
  }

  const handleAddTodo: MouseEventHandler = async (event) => {
    event.preventDefault();
    if (!!todoText && getUpdatedTodos) {
      setAddInProgress(true);
      fetch(
          `${import.meta.env.VITE_API_URL}/api/${import.meta.env.VITE_API_USER}/todos/create`,
          {
            method: "POST",
            body: JSON.stringify({ 'text': todoText }),
            headers: {
              "Content-Type": "application/json"
            },
          }
          )
        .then((res: Response)=>{
          if (!res.ok) {
            return res.json().then((errorMsg: ErrorResponse) => { throw new Error(errorMsg.error) })
          }
          return res.json()
        })
        .catch((err: Error)=>{
          console.log(err);
          setAddTodoError(`${err.name}: ${err.message}`);
          setAddInProgress(false);
          setTimeout(()=>{
            setAddTodoError(null);
          }, 4000);
        })
        .finally(()=>{
          setAddInProgress(false);
          getUpdatedTodos();
          setTodoText('');
        });
    }

  }

  return (
    <>
    <form
      className={addInProgress ? "todoItem__form_addTodo disabled" : "todoItem__form_addTodo"}
      >
      <input
        type="text"
        name="text"
        placeholder="Add your to-do ..."
        value={todoText}
        onChange={(e)=>handleChange(e)}
        disabled={addInProgress}
        />
      <button
        onClick={handleAddTodo}
        disabled={addInProgress}
        >
        Add
      </button>
    </form>
    <div
      className={addInProgress ? "todoList__dropdownFilterOverlay active" : "todoList__dropdownFilterOverlay"}
      ></div>
    {!!addTodoError &&
    <NotificationWindow notifType='error'>
      {addTodoError}
    </NotificationWindow>
    }
    </>
  )
}

const TodoItem = ({ todo }: TodoItemProps) => {
  const [todoError, setTodoError] = useState<Error | null>(null);
  const [todoIsLoading, setTodoIsLoading] = useState(false);
  const [showDeleteMenu, setShowDeleteMenu] = useState(false);
  const [itemClassString, setItemClassString] = useState('todoItem__dl_item');

  const { getUpdatedTodos } = useContext(TodosContext) || { getUpdatedTodos: null };

  const { isLoading: toggleInProgress, callback: toggleCallback } = useApiCallback(
      `${import.meta.env.VITE_API_URL}/api/${import.meta.env.VITE_API_USER}/todos/${todo._id}/toggle`,
      { method: "PUT" }
    );
  const { isLoading: deleteInProgress, callback: deleteCallback } = useApiCallback(
      `${import.meta.env.VITE_API_URL}/api/${import.meta.env.VITE_API_USER}/todos/${todo._id}`,
      { method: "DELETE" }
    );

  useEffect(()=>{
    if (deleteInProgress || toggleInProgress) {
      setItemClassString('todoItem__dl_item disabled');
    }
    else {
      setItemClassString('todoItem__dl_item active');
    }
  }, [deleteInProgress, toggleInProgress]);

  useEffect(()=>{
    setTimeout(()=>{
      setItemClassString('todoItem__dl_item active');
    }, 1000)
  }, []);

  const handleToggleCheck = async () => {
    if (getUpdatedTodos) {
      setTodoIsLoading(toggleInProgress);
      toggleCallback()
        .catch((err: Error) => {
          setTodoError(err)
        })
        .finally(()=>{
          getUpdatedTodos();
        });
      };
    }

    const handleDelete = async () => {
      if (getUpdatedTodos) {
        deleteCallback()
          .finally(()=>{
            getUpdatedTodos();
          })
      }
    }

  return (
    <div
      className={itemClassString}
      >
      <input
        type="checkbox"
        checked={todo.isDone}
        disabled={todoIsLoading || toggleInProgress || !!todoError}
        onChange={handleToggleCheck}
        />
      <dt>
        {(!todoIsLoading && !toggleInProgress) &&
        <span className={todo.isDone ? 'todoName__done' : 'todoName'}>
          {todo.text}
        </span>}
        {(todoIsLoading || toggleInProgress) && <div className='todoItem__loading'>
          <div className="loader" />
        </div>}
      </dt>
      <div className="todoItem__dl_item__options">
        <a
          onClick={()=>setShowDeleteMenu(!showDeleteMenu)}
          >
          <img src="/images/ellipsis.svg" />
        </a>
        <a
          className={showDeleteMenu || deleteInProgress ? "todoList__dropdownFilterOverlay active" : "todoList__dropdownFilterOverlay"}
          onClick={showDeleteMenu ? ()=>setShowDeleteMenu(false) : undefined}
          />
        <div
          className={showDeleteMenu ? "todoItem__deleteMenu active" : "todoItem__deleteMenu"}
          >
          <a
            onClick={handleDelete}
            >
            Delete
          </a>
        </div>
      </div>
    </div>
  )
}

const TodoList = ({ todosData }: TodoListProps) => {
  return (<dl>
    {todosData.map((todo: Todo) => <TodoItem key={todo._id} todo={todo} />)}
  </dl>)
}

const DropdownFilter = ({ currentFilter, setCurrentFilter }: DropdownFilterProps) => {
  const [showFilterList, setShowFilterList] = useState(false);

  const  selectFilter = (filter: 'All' | 'Done' | 'Undone'): void => {
    setCurrentFilter(filter);
    setShowFilterList(false);
  }

  return (<>
    <div className="todoList__dropdownFilter">
      <a
        className="filterDefault"
        onClick={()=>setShowFilterList(!showFilterList)}
        >
        {currentFilter}
      </a>
      <div
        className={showFilterList ? "todoList__dropdownFilterOverlay active" : "todoList__dropdownFilterOverlay"}
        onClick={()=>setShowFilterList(false)}
        >
      </div>
      <div
        className={showFilterList ? "todoList__dropdownFilterList active" : "todoList__dropdownFilterList"}
        >
        <a
          className={currentFilter==='All' ? 'active' : undefined}
          onClick={()=>selectFilter('All')}
          >
          All
        </a>
        <a
          className={currentFilter==='Done' ? 'active' : undefined}
          onClick={()=>selectFilter('Done')}
          >
          Done
        </a>
        <a
          className={currentFilter==='Undone' ? 'active' : undefined}
          onClick={()=>selectFilter('Undone')}
          >
          Undone
        </a>
      </div>
    </div>
  </>)
}

const TodoListContainer = () => {
  const { todos } = useContext(TodosContext) || { todos: null };
  const [currentFilter, setCurrentFilter] = useState<'All' | 'Done' | 'Undone'>('All');

  return (<StyledTodoListContainer>
    <div className="todoList__header">
      <h1>To-dos</h1>
      <DropdownFilter
        currentFilter={currentFilter}
        setCurrentFilter={setCurrentFilter}
        />
    </div>
    {todos && todos.isLoading && <div className="todoList__info">
      <div className="loader" />
    </div>}
    {todos && !todos.isLoading && !todos.error && todos.data &&
    <div className="todoList__list">
      <AddTodoForm />
      <TodoList
        todosData={todos.data.filter((todo: Todo)=>{
          if (currentFilter==='Done') {
            return todo.isDone
          }
          else if (currentFilter==='Undone') {
            return !todo.isDone
          }
          else {
            return true
          }
        })}
        />
    </div>
    }
    {todos && !todos.isLoading && todos.error &&
    <div className="todoList__info">
      <div className="errorMessage">
        <strong>{todos.error.name}:</strong>&nbsp;{todos.error.message}
      </div>
    </div>
    }
  </StyledTodoListContainer>)
}

export default TodoListContainer