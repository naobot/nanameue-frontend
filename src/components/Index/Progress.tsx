import styled from "styled-components";
import { useContext, useMemo } from "react";
import { TodosContext } from "../Index";
import { Todo } from "../../shared/interfaces";

interface ProgressBarPropTypes {
  progressValue: number;
}

const StyledProgress = styled.div`
  padding: calc(var(--global-space)*1);
  border-radius: 20px;
  background-color: #576371;
  color: #fff;

  h2 {
    color: #fff;
    font-size: 28px;
    font-style: normal;
    font-weight: 500;
    font-family: Roboto;
  }

  .progressBar__container {
    margin-top: calc(var(--global-space)*.5);
    overflow: hidden;
    border-radius: 999px;
    background-color: #fff;
    width: 100%;
  }

  .progressBar__progress {
    height: 7px;
    transition: width .2s;
    transition-timing-function: ease;
    transition-timing-function: ease-in-out;
    border-radius: 999px;
    background-color: #60c6ff;
    width: 0;
  }

  .progressBar__info {
    width: 100%;
    margin-top: calc(var(--global-space)*.5);
  }

  p {
    font-family: Roboto;
    font-size: 16px;
    color: #E0E0E0;
  }
`;

const ProgressBar = ({ progressValue }: ProgressBarPropTypes) => {
  return (
    <div className="progressBar__container">
      <div className="progressBar__progress" style={{ "width": `${progressValue}%` }}></div>
    </div>
  )
}

const Progress = () => {
  const { todos } = useContext(TodosContext) || { todos: null };
  const completedTodos: Todo[] = useMemo(()=>{
    if (todos && !todos.isLoading && todos.data) {
      return todos.data?.filter((todo: Todo) => todo.isDone);
    }
    else {
      return []
    }
  }, [todos]);

  return (
    <StyledProgress>
      <h2>Progress</h2>
      <ProgressBar
        progressValue={ todos?.data ? Math.round(100 * completedTodos.length / todos.data.length) : 0 }
        />
      <div className="progressBar__info">
      {todos && !todos.isLoading && !todos.error &&
        <p>
          {completedTodos.length} completed
        </p>
      }
      </div>
    </StyledProgress>
    )
}

export default Progress