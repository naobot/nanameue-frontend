import { PropsWithChildren, useEffect, useState } from "react";
import styled from "styled-components";

type NotificationWindowProps = PropsWithChildren<{
  notifType: 'error' | 'success';
}>;

const StyledNotificationWindow = styled.div`
  .notificationOverlay {
    position: fixed;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    background: (0,0,0,0.2);
    pointer-events: auto;
    z-index: 1;
  }
  .notificationWindow {
    z-index: 3;
    position: fixed;
    top: calc(var(--global-space)*.4);
    right: calc(var(--global-space)*.4);
    width: calc(var(--global-space)*10.5);
    font-size: 14px;
    font-weight: 600;
    padding: calc(var(--global-space)*1) calc(var(--global-space)*1);
    transform: translateY(-4px);
    transition: opacity .4s,transform .4s;
      transition-timing-function: ease, ease;
    transition-timing-function: ease-in-out;
    border-radius: 10px;
    opacity: 0;
    background-color: #fff;
    box-shadow: 0 0 8px rgba(0,0,0,.1);
    line-height: 1;
    pointer-events: none;
    will-change: opacity,transform;
    &.active {
      transform: translateY(0);
      opacity: 1;
    }
    &.success {
      color: #7fc979;
      border: 1px solid #7fc979;
    }
    &.error {
      color: #e07c7c;
      border: 1px solid #e07c7c;
    }
  }
`

const NotificationWindow = ({ notifType, ...rest }: NotificationWindowProps) => {
  const [classes, setClasses] = useState(['notificationWindow']);

  useEffect(()=>{
    let updatedClasses = ['notificationWindow', notifType];
    setTimeout(()=>{
      updatedClasses.push('active');
      setClasses(updatedClasses);
    }, 500)
    setTimeout(()=>{
      setClasses(['notificationWindow', notifType])
    }, 2500)
  }, []);

  return (<StyledNotificationWindow>
    <div className={classes.join(' ')}>
      {rest.children}
    </div>
  </StyledNotificationWindow>)
}

export default NotificationWindow