import styled from "styled-components";
import IndexWrapper from "./Index/IndexWrapper";
import { createContext, useEffect, useState } from "react";
import { Todo, TodoFetch } from "../shared/interfaces";

const StyledIndex = styled.div`
  min-height: 100dvh;
  padding: calc(var(--global-space)*3);

  @media screen
  and (max-width: 580px) {
    padding: calc(var(--global-space)*1);
  }
`;

export const TodosContext = createContext<{
  todos: TodoFetch;
  getUpdatedTodos: () => void;
}|null>(null)

const Index = () => {
  const [todos, setTodos] = useState<{ data: Todo[]; isLoading: boolean; error: Error | null }>({
    data: [] as Todo[],
    isLoading: true,
    error: null
  });

  const getUpdatedTodos = async () => {
    fetch(`${import.meta.env.VITE_API_URL}/api/${import.meta.env.VITE_API_USER}/todos`)
      .then(res => {
        if (!res.ok) {
          throw new Error(`${res.status} ${res.statusText}`)
        }
        else {
          return res.json()
        }
      })
      .then(msg => {
        setTodos({ data: msg, isLoading: false, error: null });
      })
      .catch((err: Error) => {
        setTodos({ data: [], isLoading: false, error: err });
      })
  }

  useEffect(()=>{
    getUpdatedTodos()
  }, []);

  return (<StyledIndex>
    <TodosContext.Provider
      value={
        {
          todos: todos,
          getUpdatedTodos: getUpdatedTodos
        }
      }>
      <IndexWrapper />
    </TodosContext.Provider>
  </StyledIndex>)
}

export default Index