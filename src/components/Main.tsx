import styled from "styled-components"
import Index from "./Index";

const StyledMain = styled.div`
  font-size: 18px;
  -moz-osx-font-smoothing: antialiased;
  word-break: break-word;
  overflow-wrap: break-word;
  width: 100vw;
`;

const Main = () => {
  return (
    <StyledMain>
      <Index />
    </StyledMain>
    )
}

export default Main