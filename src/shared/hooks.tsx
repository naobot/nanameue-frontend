import { useCallback, useEffect, useState } from "react";
import { Todo, CallbackFetchOptions } from "./interfaces";

export const useApiGet = (url: string): {
  data: Todo[] | null;
  isLoading: boolean;
  error: string | null;
} => {
  const [data, setData] = useState<Todo[] | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const fetchData = async (url: string ) => {
    setIsLoading(true);

    fetch(url, { method: "GET" })
    .then(res => {
      if (!res.ok) {
        throw new Error(res.statusText)
      }
      return res.json()
    })
    .then(msg => {
      setData(msg);
      setIsLoading(false);
    })
    .catch(err => {
      setError(err)
    });
  };

  useEffect(()=>{
    fetchData(url);
  }, [url]);

  return { data, isLoading, error }
}

export const useApiCallback = <T,>(
  url: string,
  options: CallbackFetchOptions,
): {
  responseStatus: number | null;
  data?: T | null | undefined;
  isLoading: boolean;
  error: string | null;
  callback: () => Promise<void>;
} => {
  const [responseStatus, setResponseStatus] = useState<number | null>(null);
  const [data, setData] = useState<T | null>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const callback = useCallback(
    async () => {
      setIsLoading(true);
      return fetch(url, options)
        .then(res => {
          if (!res.ok) {
            throw new Error(`${res.status} ${res.statusText}`)
          }
          setResponseStatus(res.status);
          setIsLoading(false);
          return res.json();
        })
        .then(msg => {
          if (msg) {
            setData(msg);
          }
        })
        .catch(err => setError(err))
    },
    [options, url]
  );

  return { responseStatus, data, isLoading, error, callback };
}