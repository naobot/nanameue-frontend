export interface Todo {
  _id: string;
  createdAt: number;
  username: string;
  isDone: boolean;
  text: string;
}
export interface TodoFetch {
  data: Todo[] | null | undefined;
  isLoading: boolean;
  error: Error | null;
}
export interface CallbackFetchOptions {
  method?: Request["method"];
  headers?: HeadersInit;
  body?: string;
  mode?: Request["mode"];
}
export interface ErrorResponse {
  error: string;
}