# To-do

This is a simple to-do list app for Nanameue's front-end app test developed by **Naomi Cui** built in React with Typescript and css-in-js styling.

The app currently assumes the username of `naomi`, which is built in as an environment variable upon deploy.

## Installation

To run the app locally in development mode, run `npm run dev` from the root directory.

## Features

In addition to the minimum specifications provided, the app comes with basic error handling, UI adjustments for loading and error states, as well as load animations to provide a comfortable amount of UX friction to the user.